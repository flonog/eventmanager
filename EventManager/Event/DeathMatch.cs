﻿using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.API;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Smod2;

namespace EventManager.Event
{
    internal class DeathMatch : IEventHandlerPlayerJoin, IEventHandlerCallCommand, IEventHandlerSetRole, IEventHandlerDoorAccess, IEventHandlerPlayerDie, IEventHandlerCheckRoundEnd, IEventHandlerRoundStart, IEventHandlerLCZDecontaminate, IEventHandlerSpawn, IEventHandlerRoundEnd, IEventHandlerCheckEscape
    {
        private Main main;
        public static bool isRoundStarted = false;

        public static Dictionary<string, int> scoreboard = new Dictionary<string, int>();

        public DeathMatch(Main main)
        {
            this.main = main;
        }
        public void OnPlayerJoin(PlayerJoinEvent ev)
        {

            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {

                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Match à Mort \n Celui qui fait le plus de kill gagne !\n Fin de la partie quand la nuke explose.", true);

                if (API.EventAPI.isRoundStarted == true)
                {
                    ev.Player.ChangeRole(Role.CLASSD);
                }

                if (!scoreboard.Keys.Contains(ev.Player.SteamId))
                {
                    scoreboard.Add(ev.Player.SteamId, 0);
                }
            }
        }

        public void OnCallCommand(PlayerCallCommandEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                if(ev.Command == "sw_join")
                {
                    if(ev.Player.TeamRole.Role != Role.CLASSD)
                    {
                        ev.Player.ChangeRole(Role.CLASSD);
                        ev.ReturnMessage = "Fait ;3";
                    }
                    ev.ReturnMessage = "<color=#000cff>OwO. Tu es déjà vivant.</color>";
                }
            }
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                if (ev.Player.TeamRole.Role != Role.CLASSD)
                {
                    ev.Role = Role.CLASSD;
                    ev.Items.Add(ItemType.MEDKIT);
                    ev.Items.Add(ItemType.E11_STANDARD_RIFLE);
                    ev.Items.Add(ItemType.LOGICER);
                }
            }
        }

        public void OnDoorAccess(PlayerDoorAccessEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                ev.Allow = true;
            }
        }

        public void OnPlayerDie(PlayerDeathEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                if(ev.DamageTypeVar == DamageType.DECONT || ev.DamageTypeVar == DamageType.FALLDOWN || ev.DamageTypeVar == DamageType.FLYING || ev.DamageTypeVar == DamageType.NUKE || ev.DamageTypeVar == DamageType.WALL || ev.DamageTypeVar == DamageType.TESLA) { return; }

                if (DeathMatchAPI.CheckScoreBoard(ev.Killer))
                {
                    scoreboard[ev.Killer.SteamId]++;
                }
                else
                {
                    DeathMatchAPI.AddPlayerToScoreboard(ev.Killer);
                    scoreboard[ev.Killer.SteamId]++;
                }
            }
        }

        public void OnCheckRoundEnd(CheckRoundEndEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                if (API.EventAPI.isNukeExplode == true)
                {
                    ev.Status = ROUND_END_STATUS.NO_VICTORY;
                }
                else
                {
                    ev.Status = ROUND_END_STATUS.ON_GOING;
                }
            }
                
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                DeathMatchAPI.SetRoomList();
                foreach(Smod2.API.Door d in PluginManager.Manager.Server.Map.GetDoors())
                {
                    if(d.Name == "SURFACE")
                    {
                        d.Locked = true;
                        d.Open = false;
                    }
                }
            }
                
        }

        public void OnDecontaminate()
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                DeathMatchAPI.SetRoomList();
            }
        }

        public void OnSpawn(PlayerSpawnEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                if(ev.Player.TeamRole.Role == Role.CLASSD)
                {
                    int roomr = new System.Random().Next(0, DeathMatchAPI.roomlist.Capacity);
                    Vector3 pos = DeathMatchAPI.roomlist[roomr].position;

                    ev.Player.Teleport(new Vector(pos.x, pos.y + 2f, pos.z));
                }
            }
        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                var sortedscoreboard = from entry in scoreboard orderby entry.Value ascending select entry;

                /*
                foreach(KeyValuePair<string, int> kv in sortedscoreboard)
                {
                    ev.Server.Map.Broadcast(10, "<color=red><b>DeathMatch</b></color> \n" + kv.Key[0] + " a gagné !", true);
                    break;
                }*/

                foreach(Player p in PluginManager.Manager.Server.GetPlayers())
                {
                    foreach(KeyValuePair<string, int> kv in sortedscoreboard)
                    {
                        if(kv.Key == p.SteamId)
                        {
                            int clas = kv.Value + 1;
                            if(clas == 1)
                            {
                                p.PersonalBroadcast(10, "<color=red><b>DeathMatch</b></color>\n Vous êtes classez 1er.", true);
                                continue;
                            }
                            p.PersonalBroadcast(10, "<color=red><b>DeathMatch</b></color>\n Vous êtes classez" + clas + "ème sur " + scoreboard.Values.Count + ".", true);
                            continue;
                        }
                    }
                }
            }
        }

        public void OnCheckEscape(PlayerCheckEscapeEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "deathmach")
            {
                ev.AllowEscape = false;
            }
        }
    }

    public static class DeathMatchAPI
    {
        public static List<Transform> roomlist;
        public const float distmin = 300;

        public static bool CheckScoreBoard(Player p)
        {
            if (DeathMatch.scoreboard.Keys.Contains(p.SteamId))
            {
                return true;
            }
            return false;
        }

        public static void AddPlayerToScoreboard(Player p)
        {
            if(CheckScoreBoard(p) == false)
            {
                DeathMatch.scoreboard.Add(p.SteamId, 0);
            }
        }

        public static void SetRoomList()
        {
            var roomlcz = GameObject.Find("LightRooms").transform;
            var roomhcz = GameObject.Find("Heavy rooms").transform;
            var roomez = GameObject.Find("EntranceRooms").transform;
            roomlist = new List<Transform>();

            if(API.EventAPI.isDecontON == false)
            {
                for (int i = 0; i < roomlcz.childCount; i++)
                {
                    Transform room = roomlcz.GetChild(i);
                    Vector3 pos = new Vector3(roomlcz.position.x, room.position.y, roomlcz.position.y);
                    if (room.name.StartsWith("Root_") && Vector3.Distance(pos, room.position) < distmin && !room.name.Contains("173"))
                    {
                        roomlist.Add(room);
                    }
                }
            }

            for (int i = 0; i < roomhcz.childCount; i++)
            {
                Transform room = roomhcz.GetChild(i);
                Vector3 pos = new Vector3(roomhcz.position.x, room.position.y, roomhcz.position.y);
                if (room.name.StartsWith("Root_") && Vector3.Distance(pos, room.position) < distmin && !room.name.Contains("Tesla"))
                {
                    roomlist.Add(room);
                }
            }

            for (int i = 0; i < roomez.childCount; i++)
            {
                Transform room = roomez.GetChild(i);
                Vector3 pos = new Vector3(roomez.position.x, room.position.y, roomez.position.y);
                if (room.name.StartsWith("Root_") && Vector3.Distance(pos, room.position) < distmin && !room.name.Contains("Shelter") && !room.name.Contains("CollapsedTunnel"))
                {
                    roomlist.Add(room);
                }
            }
        }
    }
}