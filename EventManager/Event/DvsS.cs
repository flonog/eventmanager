﻿using Smod2;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem;
using Smod2.EventSystem.Events;
using System.Timers;

namespace EventManager.Event
{
    public class DvsS : IEventHandlerPlayerJoin, IEventHandlerDecideTeamRespawnQueue
    {
        private Main main;
        private Plugin plugin;
        public DvsS(Main main)
        {
            this.main = main;
        }
        public DvsS(Plugin plugin)
        {
            this.plugin = plugin;
        }
        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "dvss")
            {
                ev.Teams = new Smod2.API.Team[] { Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST, Smod2.API.Team.CLASSD, Smod2.API.Team.SCIENTIST };
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (EventManager.API.EventAPI.eventNameOn == "dvss")
            {
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Les Scientifiques et Classes Ds se combattent entre eux.", true);
            }
        }
    }
}